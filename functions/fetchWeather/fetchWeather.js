// functions/fetchWeather.js
const fetch = require("node-fetch");

exports.handler = async function (event) {
  try {
    const country = event.queryStringParameters.country || "Antarctica";
    const API_KEY = process.env.VUE_APP_SECRET;
    const apiUrl = `https://api.openweathermap.org/data/2.5/weather?q=${country}&appid=${API_KEY}&units=metric`;

    const response = await fetch(apiUrl);
    if (!response.ok) {
      throw new Error("Network response was not ok");
    }

    const data = await response.json();
    return {
      statusCode: 200,
      body: JSON.stringify(data),
    };
  } catch (error) {
    return {
      statusCode: 500,
      body: JSON.stringify({ error: "Failed to fetch weather data" }),
    };
  }
};
