# Weather Web App

## Overview

This web application provides current weather information for countries. It displays various weather parameters such as temperature, humidity, wind speed, and more. The app is designed to be user-friendly and provides accurate and up-to-date weather data.

## Features

- **Location Search:** Users can search for Antarctica to get the current weather conditions.
- **Weather Details:** Displays overcast clouds, temperature, longitude, latitude, humidity, and wind speed.
- **Save Functionality:** Allows users to save the weather data to local storage for future reference.

## Technologies Used

- **Frontend:** HTML, CSS, JavaScript
- **API:** OpenWeatherMap API (or similar) to fetch current weather data
- **Storage:** Local Storage for saving weather data
- **Deployment:** Netlify, lambda functions

## User Interface

[https://hello-weather-simple-site.netlify.app/](https://hello-weather-simple-site.netlify.app/)

### Explanation

1. **Search Bar:** Enter "Antarctica" in the search bar to get the current weather conditions for the region.
2. **Weather Condition:** Displays the current weather condition (e.g., Overcast Clouds).
3. **Temperature:** Shows the current temperature in degrees Celsius.
4. **Location Coordinates:** Longitude and Latitude of the region.
5. **Humidity:** Displays the current humidity percentage.
6. **Wind Speed:** Shows the wind speed in meters per second.
7. **Save Button:** Save the current weather data to local storage for later use.

## Installation

1. Clone the repository:

   ```bash
   https://gitlab.com/Tobegdr/vue-weather-yes.git
   ```
